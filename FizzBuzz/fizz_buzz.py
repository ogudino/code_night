# Translates the given input x, an integer, to its Fizz Buzz representation
def translate(x):
    if x % 15 == 0:
        translation = 'Fizz Buzz'
    elif x % 3 == 0:
        translation = 'Fizz'
    elif x % 5 == 0:
        translation = 'Buzz'
    else:
        translation = str(x)

    return translation


def main():
    fizz_list = []

    for x in range(1, 100):
        translation = translate(x)
        fizz_list.append(translation)

    print(', '.join(fizz_list))


if __name__ == '__main__':
    main()
